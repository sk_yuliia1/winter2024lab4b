import java.util.Scanner;

public class VirtualPetApp {
	
	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
		
		Shark[] sharks = new Shark[2];
		
		for(int i = 0; i < sharks.length; i++) {
			
			System.out.println("Input the species of the shark: ");
			String type = reader.nextLine();
			
			System.out.println("Input the size of the shark (in feet): ");
			int size = Integer.parseInt(reader.nextLine());
			
			
			System.out.println("Input the speed of the shark: ");
			double speed = Double.parseDouble(reader.nextLine());
			
			sharks[i] = new Shark(type, size, speed);
			
			System.out.println("");
			
		}
		System.out.println(sharks[sharks.length - 1].getType());
		System.out.println(sharks[sharks.length - 1].getSize());
		System.out.println(sharks[sharks.length - 1].getSpeed());
		
		System.out.println("");
		System.out.println("Update the type: ");
		sharks[sharks.length - 1].setType(reader.nextLine());
		System.out.println("");
		
		System.out.println(sharks[sharks.length - 1].getType());
		System.out.println(sharks[sharks.length - 1].getSize());
		System.out.println(sharks[sharks.length - 1].getSpeed());
	}
	
}
