import java.util.Random;

public class Shark {
	
	private String type; // Species of shark
	private int size; // Size of sharks (meant to be in feet)
	private double speed; // The speed of the shark
	
	//constructor 
	public Shark(String type, int size, double speed){
		this.type = type;
		this.size = size;
		this.speed = speed;
	}
	
	//setter for the type field
	public void setType(String type){
		this.type = type;
	}
	
	//getter for the type field
	public String getType(){
		return this.type;
	}
	//getter for the size field
	public int getSize(){
		return this.size;
	}
	//getter for the speed field
	public double getSpeed(){
		return this.speed;
	}
	
	// Prints the type of shark and the speed of shark in one sentence
	public void swim() {
		
		System.out.println("The " + this.type + " shark is swimming at " + this.speed + " km/h!");
		
	}
	
	// Prints that the shark is munching on a random type of food
	public void eat() {
		
		Random ran = new Random();
		String[] foods = {"human", "fish", "dolphin meat", "seal meat"};
		
		String randomFood = foods[ran.nextInt(foods.length)];
		
		System.out.println("The " + this.type + " shark is munching on " + randomFood + "!");
		
	}
	
	// Checks the size of the shark, if the shark is less than 3 feet, print "cute",
	// if the shark is between 3 and 8 feet, print "could punch in the face",
	// if the shark is greater than 8 feet, print "swim for your life!"
	public void cuteOrScary() {
		
		if(this.size < 3) {
			
			System.out.println("The " + this.type + " shark is cute!");
			
		}
		
		else if(this.size >= 3 && this.size <= 8) {
			
			System.out.println("This " + this.type + " shark could be punched in the face!");
			
		}
		
		else {
			
			System.out.println("SWIM FOR YOUR LIFE!");
			
		}
		
	}	
	
}
